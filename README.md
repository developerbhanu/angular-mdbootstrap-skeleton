# MDBootstrap - Angular Skeleton

Angular CLI project with MDBootstrap and hammerjs integrated and configured.
This is just a skeleton project, which means only configurations are done but not the implementation.
You can directly start implementing MDBootstrap, no furter setup required.